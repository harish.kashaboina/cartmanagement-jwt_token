const router = require('express').Router();

const Cart = require('../../models/cart');
const Methods = require('../../methods/custom');

const apiOperation = function (req, res, crudMethod, optionsObj) {
	const bodyParams = Methods.initializer(req, Cart);
	crudMethod(bodyParams, optionsObj, (err, cart) => {
		console.log('\ncallback name\n', crudMethod.name);
		console.log('\ncart data\n', cart);
		res.send(cart);
	});
};

router.post('/getcart', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					apiOperation(req, res, Cart.getItem);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.post('/additem', (req, res) => {
	// Here body-params should contain the name of the table, cartId etc.
	// For now we are using the tablename, soon we will switch this to shopid
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
				const bodyParams = Methods.initializer(req, Cart);
				Cart.createItem(bodyParams, {
					table: req.body.table,
					overwrite: false
				}, (err, cart) => {
					console.log('\ncart data\n', cart);
					res.send(cart);
				})
			}
        })
    }
    else{
        res.send("please send a token");
    }
});

router.put('/removeitem', (req, res) => {
	// We can use '$del' as value to remove a particular attribute from the cart
	// we can pass in optionsObj too for extra control
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const optionsObj = {};
					apiOperation(req, res, Cart.updateItem, optionsObj);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.put('/updatecart', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					apiOperation(req, res, Cart.updateItem, optionsObj);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.delete('/deletecart', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					apiOperation(req, res, Cart.deleteItem, optionsObj);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

module.exports = router;